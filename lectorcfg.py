# -*- coding: utf-8 -*-
"""
Created on Sat Jul 13 17:42:39 2019

@author: L0rD Ast3r1X
"""

from configparser import ConfigParser
 
 
def read_db_config(filename='fiTracker.cfg', section='mysql'):
    """ Lee archivo de configuración y devuelve un diccionario de parámetros

    Parameters
    ----------
    filename : string, opcional
        Nombre del archivo de configuració se espera esté ubicado en el mismo directorio donde esta este archivo.
        por defecto se usa 'fiTracker.cfg'.
    section : string, opcional
        cadena de caracteres con el nombre del encabezado que debe buscar en el archivo de configuración.
        por defecto se usa 'mysql'.

    Raises
    ------
    Exception
        imprime error indicando que no encontró el encabezado

    Returns
    -------
    db : Dictionary
        devuelve un diccionario con los par+ametros de conexión

    """
    
    # crea el parser para el archivo de configuración
    parser = ConfigParser()
    parser.read(filename)
 
    # busca la sección dentro del archivo de configuración
    db = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            db[item[0]] = item[1]
    else:
        raise Exception('{0} no se halló en el archivo {1}'.format(section, filename))
 
    return db

def read_bk_config(filename='fiTracker.cfg', section='FXCM'):
    """ Lee archivo de configuración y devuelve un diccionario de parámetros

    Parameters
    ----------
    filename : string, opcional
        Nombre del archivo de configuració se espera esté ubicado en el mismo directorio donde esta este archivo.
        por defecto se usa 'fiTracker.cfg'.
    section : string, opcional
        cadena de caracteres con el nombre del encabezado que debe buscar en el archivo de configuración.
        por defecto se usa 'FXCM'.

    Raises
    ------
    Exception
        imprime error indicando que no encontró el encabezado

    Returns
    -------
    bk : Dictionary
        devuelve un diccionario con los par+ametros de conexión

    """
    print (filename)
    # crea el parser para el archivo de configuración
    parser = ConfigParser()
    parser.read(filename)
 
    # busca la sección dentro del archivo de configuración
    bk = {}
    if parser.has_section(section):
        items = parser.items(section)
        for item in items:
            bk[item[0]] = item[1]
    else:
        raise Exception('{0} no se halló en el archivo {1}'.format(section, filename))
 
    return bk

print (read_bk_config())

