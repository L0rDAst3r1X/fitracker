# -*- coding: utf-8 -*-
"""
Created on Sat Apr  3 22:40:55 2021

@author: L0rD Ast3r1X
"""


import fxcmpy 

TOKEN = '5e4fac74d7a41d87dc55f1f3945f0ef13cbf5ef6'

con = fxcmpy.fxcmpy(access_token = TOKEN, log_level = 'error')
instruments = con.get_instruments()
print(instruments[:5]) ['EUR/USD', 'USD/JPY', 'GBP/USD', 'USD/CHF', 'EUR/CHF']