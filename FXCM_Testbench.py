# -*- coding: utf-8 -*-
"""
Created on Wed Apr  8 17:22:35 2020

@author: L0rD Ast3r1X
"""


import fxcmpy
import pandas as pds
import datetime as dt
#from pylab import plt
import dbManager as mydb
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
#from fxcmpy import fxcmpy_tick_data_reader 


#############################################################################

print("version de FXCM API: ", fxcmpy.__version__)

fxcmpy_estado = "desconectado"
fxcmpy_con = None

def conectar_broker():
    """
    Función que conecta con el servidor de broker FXCM utiliza necesariamente una variable
    global 'fxcmpy_con' que usa para cargar la conexión y ademas la variable global 'fxcmpy_estado'
    para saber si la conexión ya se ha estableciodo anteriormente.tambien usa un fichero de
    configuación 'fitracker.cfg' donde se encuntrn los parametros de conexión con el servidor
    del broker.

    Returns
    -------
    estado_cnx : string
        cadena de caracteres que indica estado 'conectado' o 'desconectado'.

    """
    try:
        #declaro las variables globales para disminuir los errores
        global fxcmpy_estado
        global fxcmpy_con
        if fxcmpy_estado== "desconectado": # valido la variable que indica si la conexiónsse ha establecido antes
            #si no se ha establecido antes la conexión, lee los parametros del fichero y se conecta al servidor 
            #'demo', para entrar en producción se debe soicitar usuario para el servidor 'real'
            fxcmpy_con = fxcmpy.fxcmpy(config_file='fiTracker.cfg', server='demo') #conecta al servidor en caso de error salta a la excepción
            #si no hay error y se valido que la conexión se halla establecido correctamente y lo indico en la consola
            if fxcmpy_con.is_connected()== True:
                estado_cnx = "conectado"
                print(estado_cnx)
                print('id del usuario: %s' % fxcmpy_con.get_account_ids())#indico en la consola el id del usuario asignado por el broker
        else:
             estado_cnx = "conectado" # vuelve a asignarla a 'conectado' para no generar error de no asignación
             print(estado_cnx)
    except Exception as e:
        print(e)
        if e != "Can not connect to FXCM Server":
            fxcmpy_con.close()# si el error no es de servidor (ya coectado), desconecta y cambia el estado de la variable global 
            print("desconectando...")
            estado_cnx = "desconectado"
        elif e == "Can not connect to FXCM Server":
            estado_cnx = "conectado"# si el error es de servidor  es porque la conexió ya esta establecida
        else:
            estado_cnx = "desconectado"# cualquier otro tipo de error
        print(estado_cnx)
    return estado_cnx

def get_broker_data(con, start, stop, instrumento, periodo, numero=0):
    """
    Función que valida si la conexión con el servidor del broker está establecida, la usa para traer datos 
    históricos desde una fecha inicial a una final o un numero defiido de mediciones, con un periodo definido 
    para el instrumento indicado en los parametros, devuelve un dataframe de pandas con la información con la
    estructura, no se puede solicitar mas de 10000 instancias en cada solicitud
    ----------
    con : object
        variable que acarrea el objeto de conexión al servidor del broker
    start : datetime.datetime
        fecha de inicio en formato datetime.datetime(AAAA, M, DD)
    stop : datetime.datetime
        fecha de finalización en formato datetime.datetime(AAAA, M, DD)
    instrumento : String
        cadena de texto con el nombre del instrumento sin espacios  ('EUR/USD', 'USD/JPY', 'GBP/USD', 'GER30', 'US500')
    periodo : String
        cadena de texto con máximo tres caracteres indicando el periodo del instrumento (m1, m5... H1, H4... W1... M1)
    numero : integer, optional
        número de periodos exactos que se desean consultar, si se colca un valor en este parametro se invalida la opción 
        de fechas. el valor por defecto es 0.

    Returns
    -------
    data : dataframe
        dataframe de pandas con los resultados de la consulta o con "error"

    """
    try:
        if fxcmpy_estado == "conectado":
            instruments = con.get_instruments()
            print("instrumentos disponibles: ",instruments)
            if numero == 0:
                data = con.get_candles(instrumento, period=periodo,
                                start=start, stop=stop)
            else:
                data = con.get_candles(instrumento, period=periodo, number=numero) 
        else: 
            data = pds.DataFrame({'Error': ['error']})
    except Exception as e:
        print (e)
        data = pds.DataFrame({'Error': ['error']})
    return data


#datos = get_broker_data(fxcmpy_con, start, stop, instrumento, periodo,)


def suscribe_broker_data(con, instrumento, callbacks=(), suscribe=True):
    """
    funcion que usa la conexión al broker para hacer una suscripción a stream data de un instrumento 
    (esto da acceso a información en tiempo real).
    
    Parameters
    ----------
    con : object
        variable que acarrea el objeto de conexión al servidor del broker.
    instrumento : String
        cadena de texto con el nombre del instrumento sin espacios ('EUR/USD', 'USD/JPY', 'GBP/USD', 'GER30','US500')
    callbacks : List, optional
        lista de las funciones callback para manipular el stream data de los instrumentos. la lista esta vacia por 
        defecto. Siempre se debe finalizar la lista con una coma despuesdel último ítem y antas del parentesís ',)'
        Estas funciones se ejecutan cada vez que llega un stream de datos y solo se terminan al desuscribirse.
        los callback deben tener dos parametros uno de data para el stream y otro un dataframe de almacenamiento.
    suscribe : Boolean, optional
        Boleana que al ser cierta suscribe al istrumento y al ser falsa desuscribe.Valor por defecto es True.

    """
    try:
        if fxcmpy_estado == "conectado":
            if suscribe == True:
                con.subscribe_market_data(instrumento, callbacks)
            else:
                con.unsubscribe_market_data(instrumento)
        else:
            print("el servidor esta desconectado")
            
    except Exception as e:
        print (e)

#fxcmpy_con.subscribe_market_data('EUR/USD', (call_print_subs_data,call_plot_subs_data,))

def call_print_subs_data (data, dataframe):
    """
    funcion de callback para las suscripciones, esta imprime la información del data stream en pantalla
    se ejecuta cada vez ue llegan datos
    Parameters
    ----------
    data : object
        variable necesaria para que la FXCM API use el socket y envie información 
    dataframe : dataframe
        dataframe necesario donde se almacena la información que llega del stream de datos
    """
    print('| %3d | %s | %s | %10.5f | %10.5f | %10.5f | %10.5f |'
          % (len(dataframe), data['Symbol'],pds.to_datetime(int(data['Updated']), unit='ms'),
             data['Rates'][0], data['Rates'][1], data['Rates'][2], data['Rates'][3]))

def call_plot_subs_data (data,dataframe):
    """
    funcion de callback para las suscripciones, esta hace un plot con la información del data stream 
    solo es visualizado una vez se interrumpe la suscripción.
    se ejecuta cada vez ue llegan datos
    Parameters
    ----------
    data : object
        variable necesaria para que la FXCM API use el socket y envie información 
    dataframe : dataframe
        dataframe necesario donde se almacena la información que llega del stream de datos
    """
    dataframe.append([data['Symbol'], pds.to_datetime(int(data['Updated']), unit='ms'),
             data['Rates'][0], data['Rates'][1], data['Rates'][2], data['Rates'][3]])
    plt.style.use('seaborn')
    dataframe['Bid'].plot(figsize=(10, 6))

def call_sendsql_subs_data (data,dataframe):
    """
    funcion de callback para las suscripciones, envia la información del data stream a la base de datos
    MySQL, requiere la libreria dbManager y sus prerequisitos.
    se ejecuta cada vez ue llegan datos
    Parameters
    ----------
    data : object
        variable necesaria para que la FXCM API use el socket y envie información 
    dataframe : dataframe
        dataframe necesario donde se almacena la información que llega del stream de datos
    """
    dataframe.append([data['Symbol'], pds.to_datetime(int(data['Updated']), unit='ms'),
             data['Rates'][0], data['Rates'][1], data['Rates'][2], data['Rates'][3]])
    plt.style.use('seaborn')
    dataframe['Bid'].plot(figsize=(10, 6))

def consult_orders(con, tipo='both'):
    """
    Función que consulta las ordenes abiertas y cerradas en la plataforma del broker, devolviendo un dataframe
    con la información de las ordenes

    Parameters
    ----------
    con : object
        variable que acarrea el objeto de conexión al servidor del broker.
    tipo : string, optional
        cadena de caracteres que indica que tipo de ordenes se quieren visualizar, puede tomar valores
        'open', 'close' y 'both'. El valor por defecto es 'both'.

    Returns
    -------
    ordenes : dataframe
        devuelve un dataframe con todos los campos actualizados y una columna adiconal 'closed' booleana (1 ó 0)
        que indica si la orden ya se encuentra cerrada.

    """
    try:
         if fxcmpy_estado == "conectado" :    #comprueba la variable global que indica si hay conexión vigente con el broker    
             if tipo=='both':    
                 open_or = con.get_open_positions()
                 open_or['closed'] = ([0] * (len(open_or)))
                 print(open_or)
                 clos_or = con.get_closed_positions()
                 clos_or['closed'] = ([1] * (len(clos_or)))
                 print(clos_or)
                 ordenes= open_or
                 ordenes.append(clos_or)
             elif tipo =='open':
                 ordenes = con.get_open_positions() 
             else:
                 ordenes = con.get_closed_positions()
    
    except Exception as e:
        print (e)
        ordenes = pds.DataFrame({'Error': ['error']})
        
    return ordenes 

# print(consult_orders(fxcmpy_con))




#####################################################################################   

def calculo_periodos(inicia, finaliza, instrumento, periodo):
    #los topes para no sobrepasar los 10000 registros
    #por cada periodo estan definidos en la tabla tbl.mstr.periods
    
    
   return periodos




def plot_broker_data(con, dframe, campox1='bidopen', campox2='bidclose'):
    try:
        if fxcmpy_estado == "conectado": #comprueba la variable global que indica si hay conexión vigente con el broker
            instruments = con.get_instruments()
            print("instrumentos disponibles: ", instruments)
            
            plt.style.use('seaborn')
               # matplotlib inline
            dframe[campox1].plot(figsize=(10, 6))
            dframe[campox2].plot(figsize=(10, 6))
            
    except Exception as e:
        print (e)

#plot_broker_data(fxcmpy_con, datos )

start = dt.datetime(2019, 1, 1)
stop = dt.datetime(20, 4, 17)
instrumento = "EUR/USD"
periodo = "m15"

fxcmpy_estado = conectar_broker()

datos = get_broker_data(fxcmpy_con, start, stop, instrumento, periodo,)

print(datos)
#df= pds.DataFrame(columns= ['Symbol', 'Updated', 'Bid', 'Ask', 'High', 'Low'])
#print (df)








