# -*- coding: utf-8 -*-
"""
Created on Fri Jul 12 07:35:25 2019

@author: L0rD Ast3r1X
"""
import fxcmpy #indispensable
import mysql.connector #indispensable
import pandas as pds #indispensable
from mysql.connector import errorcode #indispensable para el manejo de las excepcioenes de conexión a base de datos
from lectorcfg import read_db_config # para pruebas de las conexiones a la base de datos BORRAR AL FINAL SI NO SE USA EN NUNGUNA FUNCION
import datetime as dt # para pruebas de las conexiones al broker BORRAR AL FINAL SI NO SE USA EN NUNGUNA FUNCION
import time # SOLO PARA BENCHMARKIN DE TIEMPOS
"""
el prefijo 'pri' significa que la función hace conexión directa con la base de datos
el refijo 'sec' significa  que la función usa una funicón primaria para ejecutar sus tareas

"""
def pri_ejecutar_query(configparams, sql, tipo='consultar'):
    """
    función que se conecta a una base de datos MySQL remota a partir de una cadena de conexión
    que debe provenir de un archivo de configuración que se encuentra en el mismo directorio
    de este archivo para eso se usa lectorcfg-read_db_config, ejecuta un query de consulta o creación  
    y devuelve dataframe de pandas

    Parameters
    ----------
    configparams : Diccionary
        Diccionario provenmiente de archivo de configuración se trae con la funcion lectorcfg.read_db_config
    sql : String
        cadena de texto del query SQL para ejecutar sobre la base de datos
    sql : String
        cadena de texto que solo debe tomar valores 'consultar' o 'crear', el valor por defecto es 'consultar'
    Returns
    -------
    result : Pandas Dataframe
        dataframe de pandas con el resultado de la consulta en esas exactas dimensiones, si se generó error en la consulta
        devuleve un dataframe de 1x1 lleno con "error"

    
        BENCHMARK DE VELOCIDAD OPERACION OK
        10.000 filas x 12 campos
        MAX = 0.3750 segs
        MIN = 0.3281 segs
        AVG = 0.3515 segs   
        
        BENCHMARK DE VELOCIDAD OPERACION ERROR
        MAX = 0.0312 segs
        MIN = 0.0156 segs
        AVG = 0.0234 segs 
        
    """
    try:
        start_time = time.process_time()# punto de inicio en el tiempo para el Benchmark 
        cnx = mysql.connector.connect(**configparams)# me conecto a la BD (cadena de conexión config file)
        mycursor = cnx.cursor() # creo el cursor
        mycursor.execute(sql) # ejecuto el query
        if tipo == 'consultar':
            result = pds.DataFrame(data = mycursor.fetchall(), 
                               columns = mycursor.column_names) #asigno los resultados a un dataframe de pandas
        else:
            result = pds.DataFrame({'consulta': ['ejecutada']}) #asigno los resultados a un dataframe de pandas
        err_flag = 0
    except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR: # error de credenciales, no se abre conexión
              err_flag = 1 
              result = pds.DataFrame({'Error': ['error']}) # asigno valor 0 al resultado para que no genere error por no asignarlo
              print("Algo esta mal con el nombre de usuario o la contraseña")
          elif err.errno == errorcode.ER_BAD_DB_ERROR: # error en parametros de consulta, se abrió la concexión
              err_flag = 0
              result = pds.DataFrame({'Error': ['error']}) # asigno valor 0 al resultado para que no genere error por no asignarlo
              print("la base de datos no existe")
          else: 
              print('error en la ejecución del comando SQL: ', err) # todo lo demas, es lo mas probable que se abra la conexión
              err_flag = 2
              result = pds.DataFrame({'Error': ['error']}) # asigno valor 0 al resultado para que no genere error por no asignarlo
    finally:
        if err_flag <1: cnx.close() # cierro la conexión si en realidad se abrió
        total_elapsed_time = round(time.process_time() - start_time, 4) # calculo el benchmark de tiempo empleado
        print("duración ",total_elapsed_time, " segs") # muestro el resultado del benchmark
    return result

#print (pri_ejecutar_consulta(read_db_config(),"SELECT * FROM `tbl.mstr.periods` WHERE 1", 'consultar'))


def pri_insertar_datos_instrumento(configparams, instrumento, periodo, tblDestino, datosInsert):
    """
    función que se conecta a una base de datos MySQL remota a partir de una cadena de conexión
    que debe provenir de un archivo de configuración que se encuentra en el mismo directorio
    de este archivo para eso se usa lectorcfg-read_db_config, inserta en la tabla tblDestino
    los datos trasladados en la variable datosInsert, se parte del principo de que los datos
    son un dataframe de pandas y mantienen la siguiente estructura:
    'Instrument', 'period','date', 'bidopen',  'bidclose', 'bidhigh', 'bidlow', 'askopen', 'askclose', 'askhigh', 'asklow', 'tickqty'
  

    Parameters
    ----------
    configparams : Diccionary
        Diccionario provenmiente de archivo de configuración se trae con la funcion lectorcfg.read_db_config
    instrumento : String
        cadena de texto con el nombre del instrumento sin espacios  ('EUR/USD', 'USD/JPY', 'GBP/USD', 'GER30', 'US500')
    periodo : String
        cadena de texto con máximo tres caracteres indicando el periodo del instrumento (m1, m5... H1, H4... W1... M1)
    tblDestino : string
        cadena de caracteres que indica el nombre de la tabla donde se van a insertar los datos
    datosInsert : dataframe
        dataframe (tabla) con los datos qeu se deben insertar

    Returns
    -------
    None.
    
    BENCHMARK DE VELOCIDAD OK
    10.000 filas x 12 campos
    MAX = 0.625 segs
    MIN = 0.4531 segs
    AVG = 0.5469 segs
    
    BENCHMARK DE VELOCIDAD ERROR
    10.000 filas x 12 campos
    MAX = 0.125 segs
    MIN = 0.0625 segs
    AVG = 0.0938 segs
    
    """
    try:
        start_time = time.process_time() # punto de inicio en el tiempo para el Benchmark 
        cnx = mysql.connector.connect(**configparams) # me conecto a la BD (cadena de conexión config file)
        mycursor = cnx.cursor() # creo el cursor
        #print ("my cursor ", mycursor.description)
        #columns = sec_nom_columns(mycursor.description)
        datos = [0] # Lista que almacenara todas las cadenas de querys para despues ejecutarlas
                    # independientemente (PROBAR LA EFICIENCIA)
        for row  in data.itertuples(index=False):
            query = ("INSERT INTO " +  tblDestino + " (`Instrument`, `period`, `date`, `bidopen`,  `bidclose`, `bidhigh`, `bidlow`, `askopen`, `askclose`, `askhigh`, `asklow`, `tickqty`) VALUES ( '"  + 
                   instrumento + "', '" + periodo +  "', '" + str(row.date) + "', '" + str(row.bidopen) + "', '" + str(row.bidclose) + "', '" + str(row.bidhigh) + "', '" + str(row.bidlow) + "', '" + 
                   str(row.askopen) + "', '" + str(row.askclose) + "', '" + str(row.askhigh) + "', '" + str(row.asklow) + "', '" + str(row.tickqty) + "')")
            datos.append (query)
        datos.pop(0)
        print ("los datos ",datos[0])
        #mycursor.execute("INSERT INTO ger30_m0 ('Instrument', 'period','date', 'bidopen',  'bidclose', 'bidhigh', 'bidlow', 'askopen', 'askclose', 'askhigh', 'asklow', 'tickqty') VALUES ( GER30 , m1 ,%s, %s, %s, %s, %s, %s, %s, %s, %s, %s )", datosInsert)
        print("done")
        for r in range (len(datos)):
            mycursor.execute(datos[r]) # ejecuto el query   
        cnx.commit()
        #result = pds.DataFrame(data = mycursor.fetchall(), 
        #                      columns = mycursor.column_names) #asigno los resultados a un dataframe de pandas
        err_flag = 0
    except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR: # error de credenciales, no se abre conexión
              err_flag = 1 
              print("Algo esta mal con el nombre de usuario o la contraseña")
          elif err.errno == errorcode.ER_BAD_DB_ERROR: # error en parametros de consulta, se abrió la concexión
              err_flag = 0
              print("la base de datos no existe")
          else: 
              err_flag = 2
              print("error en la ejecución del comando SQL: ", err) # todo lo demas, es lo mas probable que se abra la conexión
    finally:
        if err_flag <1: cnx.close() # cierro la conexión si en realidad se abrió
        total_elapsed_time = round(time.process_time() - start_time, 4) # calculo el benchmark de tiempo empleado
        print("duración ",total_elapsed_time, " segs") # muestro el resultado del benchmark

#pri_insertar_datos_instrumento(read_db_config(), 'GER30', 'm1', 'ger30_m0', data)

def pri_buscar_tabla (configparams, instrumento, periodo):
    """
    función que se conecta a una base de datos MySQL remota a partir de una cadena de conexión
    que debe provenir de un archivo de configuración que se encuentra en el mismo directorio
    de este archivo para eso se usa lectorcfg-read_db_config, valida la existencia de la tabla indicada
    indicada por los parametros, sustituye el '/' del instrumento y lo une con el epriodo co un '_'

    Parameters
    ----------
    configparams : Diccionary
        Diccionario provenmiente de archivo de configuración se trae con la funcion lectorcfg.read_db_config
    instrumento : String
        cadena de texto con el nombre del instrumento sin espacios  ('EUR/USD', 'USD/JPY', 'GBP/USD', 'GER30', 'US500')
    periodo : String
        cadena de texto con máximo tres caracteres indicando el periodo del instrumento (m1, m5... H1, H4... W1... M1)
    
    Returns
    -------
    tablaexiste : TYPE
        DESCRIPTION.
        BENCHMARK DE VELOCIDAD OK
        MAX = 0.0625 segs
        MIN = 0.0156 segs
        AVG = 0.03095 segs
        
        BENCHMARK DE VELOCIDAD ERROR
        MAX = 0.0312 segs
        MIN = 0.0156 segs
        AVG = 0.0234 segs
    """
    try:
        start_time = time.process_time()# punto de inicio en el tiempo para el Benchmark 
        cnx = mysql.connector.connect(**configparams)# me conecto a la BD (cadena de conexión config file)
        mycursor = cnx.cursor() # creo el cursor
        nombre_tabla = instrumento.replace('/', "").lower() + "_" + periodo#sustituyo el '/' agrego el '_' y paso a mminusculas
        sql=("SHOW TABLES LIKE '" + nombre_tabla +"'")
        mycursor.execute(sql) # ejecuto el query de citar las tablas
        tablaexiste = False
        result=mycursor.fetchone()
        if result != None:
            if result[0] == nombre_tabla:
                tablaexiste = True
        err_flag = 0
    except mysql.connector.Error as err:
          if err.errno == errorcode.ER_ACCESS_DENIED_ERROR: # error de credenciales, no se abre conexión
              err_flag = 1 
              tablaexiste = False # asigno valor al resultado para que no genere error por no asignarlo
              print("Algo esta mal con el nombre de usuario o la contraseña")
          elif err.errno == errorcode.ER_BAD_DB_ERROR: # error en parametros de consulta, se abrió la concexión
              err_flag = 0
              tablaexiste = False # asigno valor al resultado para que no genere error por no asignarlo
              print("la base de datos no existe")
          else: 
              print('error en la ejecución del comando SQL: ', err) # todo lo demas, es lo mas probable que se abra la conexión
              err_flag = 2
              tablaexiste = False # asigno valor al resultado para que no genere error por no asignarlo
    finally:
        if err_flag <1: cnx.close() # cierro la conexión si en realidad se abrió
        total_elapsed_time = round(time.process_time() - start_time, 4) # calculo el benchmark de tiempo empleado
        print("duración ",total_elapsed_time, " segs") # muestro el resultado del benchmark   
        
    return tablaexiste
#print(pri_buscar_tabla(read_db_config(),'GER30', 'm1'))

def sec_crear_tabla_instrumentos (configparams, instrumento, periodo):
    """
    funcion para crear tablas de instrumentos en la base de datos utilizando la funcion pri_ejecutar_query

    Parameters
    ----------
    configparams : Diccionary
        Diccionario provenmiente de archivo de configuración se trae con la funcion lectorcfg.read_db_config
    instrumento : String
        cadena de texto con el nombre del instrumento sin espacios  ('EUR/USD', 'USD/JPY', 'GBP/USD', 'GER30', 'US500')
    periodo : String
        cadena de texto con máximo tres caracteres indicando el periodo del instrumento (m1, m5... H1, H4... W1... M1)
    Returns
    -------
    None.
    
    BENCHMARK DE VELOCIDAD  PARA ERROR Y PARA OK ES  0,000000 SEGS, TAN RAPIDO COMO LA VELOCIDAD DE CONEXIÓN

    """
    try:
        start_time = time.process_time()# punto de inicio en el tiempo para el Benchmark
        nombreTabla = instrumento.replace('/', "") + "_" + periodo
        sql = ("CREATE TABLE `fitracker`.`" + nombreTabla + 
                "` ( `Instrument` VARCHAR(15) NOT NULL , `period` VARCHAR(3) NOT NULL , `date` DATE NOT NULL DEFAULT CURRENT_TIMESTAMP ," + 
                "`bidopen` DECIMAL(10,5) NOT NULL , `bidclose` DECIMAL(10,5) NOT NULL , `bidhigh` DECIMAL(10,5) NOT NULL , `bidlow` " + 
                "DECIMAL(10,5) NOT NULL , `askopen` DECIMAL(10,5) NOT NULL , `askclose` DECIMAL(10,5) NOT NULL , `askhigh` DECIMAL(10,5) " +
                "NOT NULL , `asklow` DECIMAL(10,5) NOT NULL , `tickqty` INT(5) NOT NULL , PRIMARY KEY (`Instrument`, `period`, `date`)) " +
                "ENGINE = InnoDB")
        crear = pri_ejecutar_query(configparams, sql, 'crear')
        if list(crear.columns)[0] == 'Error': #si la primera columna del dataframe se llama 'Error' aplica el valor al err_flag
            err_flag = 1
        else:
            err_flag = 0
    except Exception as e:
        err_flag = 1
        print (e,  'error segundo nivel')
    finally:
        if err_flag <1: 
            print('La tabla %s fue creada satisfactoriamente' %  nombreTabla)
        else:
            print('La tabla %s no pudo crearse' %  nombreTabla)
        total_elapsed_time = round(time.process_time() - start_time, 6) # calculo el benchmark de tiempo empleado
        print("duración ",total_elapsed_time, " segs") # muestro el resultado del benchmark
        
# sec_crear_tabla_instrumentos (read_db_config(), 'GER30', 'm1')
       


######################################################################################################################
""" funciones secundarias solo de pruena
"""

def prueba_iterativa(data):
    # los dos metodos dieron 0.06s para 12x10.000
    start_time = time.process_time()
    datos = [0]
    for(_, col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12) in data.itertuples(name=None):
    #for row in data.itertuples(index=False):
         datos.append ([col1, col2, col3, col4, col5, col6, col7, col8, col9, col10, col11, col12])
         #datos.append ([row.Instrument, row.period, row.date, row.bidopen, row.bidclose, row.bidhigh, row.bidlow, row.askopen, row.askclose, row.askhigh, row.asklow, row.tickqty])
    print(datos)  
    total_elapsed_time = round(time.process_time() - start_time, 4)
    print("duración ",total_elapsed_time, " segs")
     
def sec_nom_columns (datos):
    ### función secundaria que toma la descripción de los resultados de una consulta ejecutada por un cursor 
    # para sacarle los nombres de columna y poder devolverlos a la funcion primaria
    try:
        print ("datos ",datos)
        if datos == None :
                nom_columnas = ("'instrument', 'period', 'date', 'bidopen',  'bidclose',  'bidhigh',  'bidlow', 'skopen', 'askclose',  'askhigh', 'asklow',  'tickqty'")
        else:
            for fields in datos: # asigna los campos de los datos a un tuple donde solo se toma el campo 0  del nombre
                nom_columnas = fields[0]
    except Exception as e:
        print (e,  'error segundo nivel')
    return nom_columnas
    
    
data = pds.read_csv(r'C:\Users\L0rD Ast3r1X\Documents\Python Scripts\FITracker\ger30_m2.csv')
data['Instrument'] = (['GER30'] * (len(data)))
data['period'] = (['m1'] * (len(data)))
data = data[['Instrument', 'period', 'date', 'bidopen',  'bidclose', 'bidhigh', 'bidlow', 'askopen', 'askclose', 'askhigh', 'asklow', 'tickqty']]
print (data)


    
#data = con.get_candles('GER30', period='m1', number=10000)
#pds.DataFrame 
#data.to_csv(r'C:\Users\L0rD Ast3r1X\Documents\Python Scripts\FITracker\ger30_m1.csv', index = False)





